# Kisphp Kubernetes manifests compiler (similar to helm charts)

## Install 

```bash
pip install git+https://gitlab.com/kisphp/k4p.git
```

## Compile local manifests

```bash
k4p build {env}

# compile, change variable and show output
k4p build prod --set image_tag=123132 -s

# compile, change variable
k4p build prod --set image_tag=123132
```
## Generate manifests

```bash
k4p template {env}

# compile, change variable and show output
k4p template prod --set image_tag=123132 -s

# compile, change variable
k4p template prod --set image_tag=123132
```

## Kubernetes Directory structure

```bash
kube
├── envs
│   ├── prod.yaml
│   └── test.yaml
├── manifests
│   ├── config-map.yaml
│   ├── deployment.yaml
│   ├── ingress.yaml
│   ├── secrets.yaml
│   └── service.yaml
└── values.yaml
```

The script will generate a `kube/app.yaml` file that contains all kubernetes manifests defined and enabled
