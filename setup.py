import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

files = ["src/"]

setuptools.setup(
    name="k4p",
    version="0.1.0",
    author="Bogdan Rizac",
    author_email="367815-marius-rizac@users.noreply.gitlab.com",
    description="K8S manifests manager",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/kisphp/k4p.git",
    packages=setuptools.find_packages(),
    install_requires=[
        'cleo==0.8.1',
        'clikit==0.6.2',
        'crashtest==0.3.1',
        'pastel==0.2.1',
        'pylev==1.3.0',
        'pyyaml==5.4.1',
        'jinja2==3.0.1',
    ],
    scripts=['src/app.py'],
    entry_points={
        'console_scripts': [
            'k4p = app:entrypoint'
        ]
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.7',
)
