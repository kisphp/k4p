#!/usr/bin/env python3

from cleo import Application
from src.commands import Build, Template

def entrypoint():
    app = Application()
    app.add(Build())
    app.add(Template())

    app.run()


if __name__ == '__main__':
    entrypoint()
