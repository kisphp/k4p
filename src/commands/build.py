import os
import yaml
import base64

from cleo import Command
from jinja2 import Environment, select_autoescape, FileSystemLoader

KUBE_ENVS = 'envs'
KUBE_MANIFESTS = 'kube/manifests'
APP_YAML = 'app.yaml'
GLOBAL_VALUES_FILE = 'values.yaml'


def filter_b64encode(text):
    return base64.b64encode(text.encode('utf8')).decode('utf8')


def filter_b64decode(text):
    return base64.b64decode(text.encode('utf8')).decode('utf8')


def filter_quoted(text):
    return f'"{str(text)}"'


class Build(Command):
    """
    Build kubernetes templates

    build
        {env? : Select environment}
        {--c|config=? : Configuration yaml file}
        {--s|show : Show compiled manifests on screen}
        {--set=? : Set variables}
    """
    def handle(self):
        env = self.argument('env')
        set_options = self.option('set')

        self.manifests = []
        self.variables = {
            'app': 'demo',
            'namespace': 'default',
            'image_tag': 'latest',
            'deployment': { 'enabled': False },
            'service': { 'enabled': False },
            'config_map': { 'enabled': False },
            'secrets': { 'enabled': False },
            'ingress': {
                'enabled': False,
                'tls': {
                    'enabled': False
                },
            },
        }

        self._parse_global_values_file()
        self._parse_environment_file(env)
        self._set_custom_options(set_options)
        self._read_manifest_files()
        self._write_compiled_file()

        self._output(f'K8S manifests generated at {APP_YAML}')

    def _parse_global_values_file(self):
        values_file = GLOBAL_VALUES_FILE
        config_file = self.option('config')
        if config_file:
            values_file = config_file

        try:
            with open(values_file) as f:
                self.variables.update(yaml.safe_load(f))
        except FileNotFoundError:
            pass

    def _parse_environment_file(self, env):
        if env:
            with open(f'{KUBE_ENVS}/{env}.yaml') as f:
                variables_env = yaml.safe_load(f)
                self.variables.update(variables_env)

    def _set_custom_options(self, set_options):
        if set_options is not None:
            extra = set_options.split(';')
            if len(extra) > 0:
                for option in extra:
                    key, val = option.split('=')
                    self.variables.update({key: val})

    def _read_manifest_files(self):
        manifests_path = f'{os.path.dirname(os.path.dirname(os.path.dirname(__file__)))}/{KUBE_MANIFESTS}'

        jenv = Environment(
            loader=FileSystemLoader([manifests_path]),
            autoescape=select_autoescape()
        )
        jenv.filters['b64encode'] = filter_b64encode
        jenv.filters['b64decode'] = filter_b64decode
        jenv.filters['quoted'] = filter_quoted

        for (_, _, filenames) in os.walk(manifests_path):
            for file in filenames:
                tpl = jenv.get_template(file)
                self._output(f'-> {file}')
                k8s_content = tpl.render(**self.variables)
                if k8s_content:
                    self.manifests.append(k8s_content)

    def _write_compiled_file(self):
        k8s_content = "\n".join(self.manifests)
        if not self.option('show'):
            with open(APP_YAML, 'w') as fw:
                fw.write(k8s_content)
            return None

        self.line(k8s_content)

    def _output(self, message):
        if not self.option('show'):
            self.line(message)
