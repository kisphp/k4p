import os
import yaml
import base64

from cleo import Command
from jinja2 import Environment, select_autoescape, FileSystemLoader

KUBE_ENVS = 'kube/envs'
KUBE_MANIFESTS = 'kube/manifests'
APP_YAML = 'kube/app.yaml'
GLOBAL_VALUES_FILE = 'kube/values.yaml'

def filter_b64encode(text):
    return base64.b64encode(text.encode('utf8')).decode('utf8')

def filter_b64decode(text):
    return base64.b64decode(text.encode('utf8')).decode('utf8')

class Template(Command):
    """
    Build kubernetes templates

    template
        {env? : Select environment}
        {--set=? : Set variables}
        {--s|show : Show compiled manifests on screen}
    """
    def handle(self):
        env = self.argument('env')
        set_options = self.option('set')

        self.variables = {}
        self.manifests = []

        self._parse_global_values_file()
        self._parse_environment_file(env)
        self._set_custom_options(set_options)
        self._read_manifest_files()
        self._write_compiled_file()

        self._output(f'K8S manifests generated at {APP_YAML}')

    def _parse_global_values_file(self):
        with open(GLOBAL_VALUES_FILE) as f:
            self.variables = yaml.safe_load(f)

    def _parse_environment_file(self, env):
        if env:
            with open(f'{KUBE_ENVS}/{env}.yaml') as f:
                variables_env = yaml.safe_load(f)
                self.variables.update(variables_env)

    def _set_custom_options(self, set_options):
        if set_options is not None:
            extra = set_options.split(';')
            if len(extra) > 0:
                for option in extra:
                    key, val = option.split('=')
                    self.variables.update({key: val})

    def _read_manifest_files(self):
        jenv = Environment(
            loader=FileSystemLoader(["kube/manifests"]),
            autoescape=select_autoescape()
        )
        jenv.filters['b64encode'] = filter_b64encode
        jenv.filters['b64decode'] = filter_b64decode

        for (_, _, filenames) in os.walk(KUBE_MANIFESTS):
            for file in filenames:
                tpl = jenv.get_template(file)
                self._output(f'-> {file}')
                self.manifests.append(tpl.render(**self.variables))

    def _write_compiled_file(self):
        k8s_content = "\n".join(self.manifests)
        with open(APP_YAML, 'w') as fw:
            fw.write(k8s_content)

        if self.option('show'):
            print(k8s_content)

    def _output(self, message):
        if not self.option('show'):
            self.line(message)